FactoryGirl.define do
  factory :user do
    first_name "Default"
    last_name "User"
    password "12345678"
    roles ["client"]

    sequence(:email) { |number| "user#{number}@example.com" }

    factory :client_user do
      sequence(:first_name) { |number| "Client #{number}" }
      roles ["client"]
      sequence(:email) { |number| "client#{number}@example.com" }
    end

    factory :admin_user do
      sequence(:first_name) { |number| "Admin #{number}" }
      roles ["admin"]
      sequence(:email) { |number| "admin#{number}@example.com" }
    end

    factory :moderator_user do
      sequence(:first_name) { |number| "Moderator #{number}" }
      roles ["moderator"]
      sequence(:email) { |number| "moderator#{number}@example.com" }
    end

  end
end
