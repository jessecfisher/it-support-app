Given /^the following user records?$/ do |table|
  User.delete_all
  table.hashes.each do |hash|
    hash["roles"] = hash["roles"].split
    user = FactoryGirl.create(:user, hash)
  end
end

When(/^I go to the manage users page$/) do
  visit users_path
end

Then(/^I should see "(.*?)"$/) do |arg1|
  page.should have_content(arg1)
end

