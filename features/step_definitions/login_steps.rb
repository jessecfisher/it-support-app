Given /^I have logged in as "(.*)" with password "(.*)"$/ do |username, password|
  visit user_session_path
  fill_in "Email", :with => username
  fill_in "Password", :with => password
  click_button "Sign in"
end

Given(/^a valid (.*) user$/) do |role|
  @user = FactoryGirl.create "#{role}_user"
end

When(/^I go to the login page$/) do
  visit user_session_path
end

When(/^I fill in the login info$/) do
  fill_in "Email", :with => @user.email
  fill_in "Password", :with => @user.password
end

When(/^I press "(.*?)"$/) do |button_text|
  click_button button_text
end

Then(/^I should see the admin dashboard page$/) do
  page.should have_content("Admin Dashboard")
end

Then(/^I should see the moderator dashboard page$/) do
  page.should have_content("Moderator Dashboard")
end

Then(/^I should see the client dashboard page$/) do
  page.should have_content("Client Dashboard")
end

Then(/^I should not see the admin dashboard page$/) do
  page.should_not have_content("Admin Dashboard")
end
