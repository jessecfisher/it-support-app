Given(/^(\d+) client users with (\d+) tickets each$/) do |users, tickets|
  1.upto(users.to_i) do |user|
    @user = FactoryGirl.create :client_user
    1.upto(tickets.to_i) do |ticket|
      FactoryGirl.create :ticket, user: @user
    end
  end
end

When(/^the first client logs in$/) do
  visit user_session_path
  fill_in "Email", :with => User.first.email
  fill_in "Password", :with => "12345678"
  click_button "Sign in"
end

When(/^the admin user logs in$/) do
  visit user_session_path
  fill_in "Email", :with => User.find_by(roles_mask: 1).email
  fill_in "Password", :with => "12345678"
  click_button "Sign in"
end

Then(/^he should see his tickets$/) do
  page.should have_content(User.first.first_name)
end

Then(/^he should not see the other client's tickets$/) do
  page.should_not have_content(User.last.first_name)
end

Then(/^he should see all the tickets$/) do
  @users = User.where(roles_mask: 4)
  @users.each do |user|
    page.should have_content("#{user.full_name}")
  end
end
