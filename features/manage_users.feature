Feature: Manage users
  In order to be able to manage the users
  As the administrator of this app
  I want to add, delete and edit accounts of users

  Background:
    Given the following user records
      | email              | password | roles     | first_name | last_name |
      | admin@example.com  | 12345678 | admin     | Admin      | User      |
      | tech@example.com   | 12345678 | moderator | Tech       | User      |
      | client@example.com | 12345678 | user      | Client     | User      |
    And I have logged in as "admin@example.com" with password "12345678"

  Scenario: Show all accounts
    When I go to the manage users page
    Then I should see "admin@example.com"
    And I should see "tech@example.com"
    And I should see "client@example.com"
