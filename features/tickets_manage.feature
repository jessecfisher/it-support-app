Feature: View, add, close, remove tickets
  In order to manage my tickets
  As a client
  I want to view, add, close, and remove my tickets

  Background:
    Given 3 client users with 3 tickets each
    Given a valid admin user

  Scenario: A logged in client user manages their tickets from their dashboard
    When the first client logs in
    Then he should see his tickets
    And he should not see the other client's tickets

  Scenario: An admin user manages tickets from their dashboard
    When the admin user logs in
    Then he should see all the tickets
