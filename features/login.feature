Feature: Login
  Scenario: Admin user logs in and is directed to their dashboard
    Given a valid admin user
    When I go to the login page
    And I fill in the login info
    And I press "Sign in"
    Then I should see the admin dashboard page

  Scenario: Client user logs in and is directed to their dashboard
    Given a valid client user
    When I go to the login page
    And I fill in the login info
    And I press "Sign in"
    Then I should see the client dashboard page
    And I should not see the admin dashboard page

  Scenario: Moderator user logs in and is direted to their dashboard
    Given a valid moderator user
    When I go to the login page
    And I fill in the login info
    And I press "Sign in"
    Then I should see the moderator dashboard page
    And I should not see the admin dashboard page
