require 'spec_helper'

describe "tickets/new" do
  before(:each) do
    assign(:ticket, stub_model(Ticket,
      :title => "MyString",
      :body => "MyText",
      :user => nil
    ).as_new_record)
  end

  it "renders new ticket form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", tickets_path, "post" do
      assert_select "input#ticket_title[name=?]", "ticket[title]"
      assert_select "textarea#ticket_body[name=?]", "ticket[body]"
      assert_select "input#ticket_user[name=?]", "ticket[user]"
    end
  end
end
