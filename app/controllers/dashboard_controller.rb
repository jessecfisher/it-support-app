class DashboardController < ApplicationController
  before_filter :authenticate_user!

  def index
    if current_user.has_role? :client
      @tickets = current_user.tickets
    elsif current_user.has_role? :admin or current_user.has_role? :moderator
      @tickets = Ticket.all
    end
  end

end
