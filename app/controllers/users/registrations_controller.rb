class Users::RegistrationsController < Devise::RegistrationsController
  def resource_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :current_password)
  end

  def update

    if resource.update_without_password(resource_params)
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case his password changed
      #sign_in @user, :bypass => true
      render "edit"
    end

  end

  private :resource_params
end
