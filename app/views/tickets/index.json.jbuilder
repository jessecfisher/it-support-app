json.array!(@tickets) do |ticket|
  json.extract! ticket, :title, :body, :user_id, :created_at, :updated_at
  json.url user_ticket_url(ticket.user, ticket, format: :json)
end
